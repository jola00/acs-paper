\section{Sicherheit in VANETs \cite{gauher_security_2020}}
Die Kommunikation in VANETs ist sehr anfällig für verschiedene Attacken, wie z. B. Replay-Attacken, umbemerkte Manipulation von Nachrichten und Missbrauch von Fahrzeugidentitäten.
Diese Attacken können katastrophale Folgen, wie schwere Unfälle, nach sich ziehen.\\
Um solche Folgen zu verhindern, wurden bereits einige Kommunikationsschemata entwickelt.
Jedoch erfüllen all diese, aufgrund des kryptographischen Overheads, die Effizienzanforderungen an zukünftige VANETs nicht.
Daher entwickelten Gauher et al. ein neuartiges Kommunikationsprotokoll basierend auf einer \acf{PKI}, welches diesen Anforderungen gerecht wird.
Dieses basiert auf dem \ac{CMDS} von Jia et al. \cite{wang_cmds_2017}.\\
Zuerst wird die dabei grundlegende Architektur für VANETs vorgestellt.
Anschließend wird die Vehicle Registration number (VRn), die zur Zuordnung digitaler Zertifikate zu Fahrzeugen dient, eingeführt.
Zuletzt wird das daraus resultierende Kommunikationsschema erläutert.\\
Im Folgenden soll nur die Verteilung von \textit{Safety}-Nachrichten betrachtet werden.

\subsection{Architektur}
In dieser Betrachtung wird eine vereinfachte Architektur ohne den Einsatz von Edge Cloud Computing angenommen.
Es handelt sich dabei um eine Drei-Schicht-Architektur, welche in Abbildung~\ref{fig:sec_arch} dargestellt ist.
\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.5\linewidth]{./img/sec_archi.png}
    \caption{VANET-Architektur für sichere Kommunikation \cite{gauher_security_2020}}
    \label{fig:sec_arch}
\end{figure}

In der untersten Ebene befinden sich die Fahrzeuge als Datensammler.
Die mittlere Ebene umfasst (Personen-)Busse, die als Gateways zur zentralen Cloud dienen.
Letztere ist für die Verteilung von \textit{Safety}-Nachrichten zuständig und befindet sich in der Architektur auf der obersten Ebene.\\
Busse als Gateways zu verwenden ist vorteilhaft, da diese in einem großen Bereich unterwegs sind und oft bereits Mobilfunktechnologie verbaut haben.
Somit können sie in einem großen Bereich Fahrzeugen Internetzugang zur Verfügung stellen.
Die Gateways müssen sich, bevor sie an der Kommunikation teilnehmen, an der zentralen Cloud anmelden.
Dadurch wird ihnen eine ID zugewiesen, welche sie für den folgenden Nachrichtenaustausch nutzen.
In regelmäßigen Zeitintervallen sendet jedes Gateway Informationen, z. B. zum Standort, an die Nachbargateways und die zentrale Cloud, damit diese sich ein Bild über die Verkehrslage machen kann.\\
Da die Busse nicht immer gleichverteilt sind, bestimmt die Cloud kontinuierlich die optimale Auswahl dieser.
Dabei dürfen diese nicht zu dicht beieinander sein, aber auch die Kommunikationsbandbreite pro Bus ist zu beachten.
Im Folgenden soll allerdings angenommen werden, dass die Busse Poisson-verteilt sind, damit das Kommunikationsschema getestet werden kann.

\subsection{\ac{VRn}}
Die \ac{VRn} ist eindeutig pro Fahrzeug und wird durch eine Behörde, wie z. B. das \ac{DMV} in den USA, einem Fahrzeug zugeordnet und eincodiert.
Diese Nummer kann zum Abruf eines digitalen Zertifikates verwendet werden.\\
Aufgrund der Beteiligung einer öffentlichen Behörde, kann die Nachahmung der Identität von Fahrzeugen verhindert werden.
Wenn ein Fahrzeug versucht sich zu registrieren, dann prüft die \ac{RA} die \ac{VRn} bei der Behörde.
Falls es sich um ein bösartiges Fahrzeug handelt, welches nicht registriert ist, so wird kein digitales Zertifikat ausgegeben.

\subsection{Protokoll}
Abbildung~\ref{fig:scheme} zeigt das Kommunikationsschema in einer solchen Architektur in Kombination mit der \ac{VRn} zum Abruf des digitalen Zertifikats.
\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.6\linewidth]{./img/scheme.png}
    \caption{Kommunikationsschema in VANET mit PKI nach \cite{gauher_security_2020}}
    \label{fig:scheme}
\end{figure}

Dabei wird in die drei Kommunikationspartner Fahrzeug (\textit{SV}), die Registration Authority (\textit{RA}) und das Gateway (\textit{GW}) unterteilt.
Jeder dieser besitzt jeweils ein Paar aus öffentlichem und privatem Schlüssel.
Bei der Registrierung sendet das Fahrzeug eine Anfrage für ein digitales Zertifikat an die Registration Authority.
Diese Anfrage enthält die VRn des anfragenden Fahrzeugs (Schritt 1) in Abb.~\ref{fig:scheme}). 
Nach Empfangen der Anfrage prüft die Registration Authority die VRn in ihrer Datenbank.
Falls die VRn ungültig ist, wird keine Aktion ausgelöst.\\
Anderenfalls, bei gültiger VRn, signiert die Registration Authority den öffentlichen Schlüssel des neuen Fahrzeugs mit ihrem privaten Schlüssel und leitet diesen an die Gateways weiter (Schritt 2) in Abb.~\ref{fig:scheme}).
Das Gateway empfängt den signierten öffentlichen Schlüssel des Fahrzeugs und überprüft diesen mit dem öffentlichen Schlüssel der Registration Authority.
Falls die Überprüfung erfolgreich war, speichert das Gateway diesen Schlüssel und nutzt ihn zur Authentifizierung des Fahrzeugs.\\
Wenn das Fahrzeug nun eine \textit{Safety}-Nachricht versenden möchte, so erstellt es diese und generiert deren Hash-Wert.
Sowohl der generierte Hash-Wert, als auch ein aktueller Zeitstempel werden mit dem privaten Schlüssel des Fahrzeugs signiert und an die Nachricht angehangen.
Anschließend wird die Nachricht an das nächstgelegene Gateway versendet (Schritt 3) in Abb.~\ref{fig:scheme}).\\
Das empfangende Gateway prüft nach Erhalt der Nachricht die Authentizität dieser durch die Prüfung der Signatur des Hash-Wertes und des Zeitstempels.
Durch den Vergleich des übermittelten Hash-Wertes und des neu berechneten Hash-Wertes kann zudem die Integrität der Nachricht sichergestellt werden.
Zuletzt wird der Zeitstempel geprüft, um Replay-Attacken zu vermeiden.\\
Die Autoren in \cite{gauher_security_2020} zeigen zudem mit dem Protokoll-Prüfer \textit{Scyther}, dass das vorgestellte Protokoll valide ist.