\section{Cloud-Technologien in VANETs \cite{gazi_challenges_2018}}
Dieses Kapitel untersucht den Einsatz verschiedener Edge Cloud Computing Technologien in VANETs.\\
Dazu werden erst die Herausforderungen zukünftiger VANETs genannt. 
Anschließend folgt ein Vergleich verschiedener Technologien.
Abschließend wird die am besten geeignete Technologie an einem Beispiel erläutert.

\subsection{Herausforderungen und Anforderungen zukünftiger VANETs}
Zur Kontrolle des Verkehrsfluss werden in VANETs sogenannte \textit{Safety-Nachrichten} versendet. 
Diese dürfen allerdings nur an Fahrzeuge im Bereich des zu kommunizierenden Verkehrsereignisses zugestellt werden.
Daraus ergeben sich folgende Herausforderungen:
\begin{enumerate}
    \item \textit{Unterbrochene Verbindungen:} Hierzu zählen sowohl komplett unterbrochene Verbindungen, als auch Verbindungen mit hohem Paketverlust.
    \item \textit{Gute Ortskenntnis:} VANETs müssen in der Lage sein die Position jedes Fahrzeug richtig zu bestimmen, damit Nachrichten korrekt zugestellt werden können.
    \item \textit{Heterogenes Fahrzeugmanagement:} Zukünftige VANET-Technologien müssen mit technischen Unterschieden in Fahrzeugen verschiedener Hersteller zurechtkommen.
    \item \textit{Datensicherheit:} Um die Privatsphäre der Nutzer zu schützen, müssen VANETs zulassen, dass Daten je nach Sensitivität lokal oder in der Cloud ausgewertet werden.
    \item \textit{Unterstützung von Netzwerkintelligenz:} VANET-Technologien müssen in der Zukunft das Sammeln und Auswerten von Daten durch die Edge Server ermöglichen, bevor diese beispielsweise mit den zentralen Cloud-Servern geteilt werden.
\end{enumerate}
Somit ist eine geringe Latenz erforderlich um Echtzeitanwendungen, wie die Zustellung von \textit{Safety-Nachrichten} zu ermöglichen.
Außerdem müssen zukünftige VANETs eine hohe Bandbreite für Infotainment-Anwendungen bieten.
Zuletzt sollten VANETs dauerhafte und zuverlässige Verbindungen zwischen den Fahrzeugen und stationären Einheiten sicherstellen.

\subsection{Edge Cloud Computing}
Das \ac{ECC} erweitert das Konzept der konventionellen Cloud durch eine weitere Schicht, die sogenannten Edge-Geräte - geografisch verteilte Mikro-Datenzentren.
\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.57\linewidth]{./img/fog.png}
    \caption{Darstellung der Edge Cloud Computing Architektur mit Vergleich der Anzahl der Knoten, der Bandweite und der Latenz \cite{gazi_challenges_2018}}
    \label{fig:edgecloud}
\end{figure}

In Abbildung~\ref{fig:edgecloud}, in der die Drei-Schicht-Architektur dargestellt ist, kann man erkennen, dass mit dem Abstand zwischen Fahrzeug und Verarbeitungsknoten die Anzahl der zu traversierenden Knoten steigt.
Damit steigt die Latenz und es verringert sich die Bandbreite.
Daher bieten die Edge-Geräte eine geringere Latenz und höhere Bandbreite als die konventionelle Cloud.
Edge Cloud Computing führt die Speicherung und Auswertung von Daten verteilt aus, wodurch nicht jedes Datenpaket an die zentrale Cloud weitergeleitet werden muss.
Solche Ansätze werden aufgrund der Möglichkeit von On-Demand-Migration von Anwendungscode meist mit \ac{IaaS} umgesetzt.

Es gibt verschiedene Konzepte, wie Edge Cloud Computing umgesetzt werden kann.
Im Folgenden werden die drei ähnlichen Konzepte Fog Computing, \ac{MEC} und Cloudlets vorgestellt und verglichen.\\
Die Standardisierung und Entwicklung von \textbf{Fog Computing} wird durch das OpenFog Consortium vorangetrieben.
Wie in den anderen Konzepten, befinden sich die Fahrzeuge, die als Datensammler dienen, auf der untersten Ebene.
Die Fog-Plattform ist in der Mitte der Drei-Schicht-Architektur und umfasst die \ac{RSU}s, sowie die Fog Server und die Infrastruktur für das kabellose Netzwerk.
Auf der obersten Ebene befindet sich die zentrale, skalierbare Cloud.
Damit ermöglicht Fog Computing geringe Latenz in Kombination mit hoher Bandbreite und hohem \ac{QoS}.\\
Auch \textbf{\acf{MEC}} ist standardisiert, damit Anwendungen verschiedener Service-Anbieter und Netzbetreiber effizient und nahtlos integriert werden können.
Bei \ac{MEC} wird die bestehende Molbilfunk-Infrastruktur verwendet um Cloud-Services verteilt zur Verfügung zu stellen.
Die zentralen \ac{MEC}-Server, welche Rechenleistung, Speicherplatz, Verbindung zur zentralen Cloud und Informationen zur Mobilfunkzelle bereitstellen, werden in den Basisstationen des Mobilfunknetzes eingerichtet. \\
\textbf{Cloudlets} werden durch die \ac{OEC} Initiative standardisiert.
Ein Cloudlet ist in der Mitte der Drei-Schicht-Architektur angeordnet und kann als Proxy zur zentralen Cloud verstanden werden.
Es stellt im Falle der Nichtverfügbarkeit der zentralen Cloud essentielle Cloud-Services bereit.
Daher wird es meist unter instabilen Verbindungsverhältnissen verwendet.
\begin{table}[H]
    \caption{Vergleich von Fog Computing, \acf{MEC}, Cloudlets und der konventionellen Cloud (angelehnt an \cite{gazi_challenges_2018})}
    \begin{tabular}{|p{3.2cm}|p{2.6cm}|p{2.6cm}|p{2.6cm}|p{3.6cm}|}
        \hline
        \textbf{Vergleichskriterium} & \textbf{Fog Computing} & \textbf{\ac{MEC}} & \textbf{Cloudlets} & \textbf{Konventionelle Cloud}  \\ \hline
        \textit{\textbf{Geringe Latenz / Jitter}} & Ja & Ja & Ja & Nein \\ \hline
        \textit{\textbf{Netzwerkarchitektur}} & Dezentralisiert & Dezentralisiert & Dezentralisiert & Zentralisiert \\ \hline
        \textit{\textbf{Unterstützung von Anwendungen mit Mobilität}} & Ja & Ja & Nicht inhärent, sollte aber möglich sein & Nein \\ \hline
        \textit{\textbf{Betonung auf Online-Analysen und Interaktion mit der Cloud}} & Ja & Ja & Nicht angegeben & Ja \\ \hline
        \textit{\textbf{Standort der Edge Server}} & Flexibel & Bei Basisstationen & Rand des Netzwerks & Nicht angegeben \\ \hline
        \textit{\textbf{Dynamische Broadcast-Reichweite abhängig von Nachrichtentyp}} & Ja & Teilweise, aber nicht dynamisch & Ja & Nein \\ \hline
        \textit{\textbf{Möglichkeit zur Erfassung von Standorten}} & Ja & Ja & Ja & Nein \\ \hline
        \textit{\textbf{Kanalbandbreite}} & 75 MHz & 20 MHz & 20 MHz & -- \\ \hline
        \textit{\textbf{Durchsatz}} & 6 Mbps & 150 Mbps Downlink \& 50 Mbps Uplink & 50 Mbps & --  \\ \hline
        \textit{\textbf{Skalierbarkeit}} & Hoch & Hoch & Hoch & Durchschnittlich \\ \hline
    \end{tabular}
    \label{tab:vgl}
\end{table}
Ein umfangreicher Vergleich der drei Edge Cloud Computing Technologien und der konventionellen Cloud ist in Tabelle~\ref{tab:vgl} zu sehen.
Die drei Edge Cloud Computing Technologien haben, aufgrund der Nähe der Edge Server zu den Endgeräten, gemein, dass sie eine geringe Latenz ermöglichen.
Während Fog Computing und \ac{MEC} Anwendungen mit Mobilität unterstützen, wurde dies bei Cloudlets bisher nicht betrachtet.
Zudem wird bei der Cloudlet-Technologie, anders als bei Fog Computing und \ac{MEC}, nicht erwähnt, dass die Analyse großer Datenmengen unterstützt wird.\\
Bei Fog Computing können die Edge-Server an einer beliebigen Stelle zwischen den Fahrzeugen und der Cloud positioniert werden.
\ac{MEC}-Server müssen zwingend bei Basisstationen oder anderen Standorten der Mobilfunkinfrastruktur aufgebaut sein.
Die Edge-Server-Position bei Cloudlets ist nicht genau definiert.\\
Daraus ergibt sich, dass Fog Computing eine dynamische Broadcast-Reichweite ermöglicht.
Aufgrund der Ausrichtung an den Mobilfunkzellen, ist die Broadcastreichweite bei \ac{MEC} nicht dynamisch. 
Alternativ zur Zustellung der Nachrichten in der gesamten Mobilfunkzelle, können Suchoperationen zur Auswahl der betroffenen Geräte durchgeführt werden.
Diese sind jedoch sehr komplex und werden durch die Vielfalt an Netzanbietern erschwert.\\
Dank der Verwendung des \ac{WAVE}-Protokolls und der damit verbundenen großen Kanalbandbreite, ist Fog Computing unanfälliger gegenüber Funkstörungen.
Allerdings hat Fog Computing unter den drei Edge Cloud Computing Technologien den geringsten Durchsatz.\\
Dieser Vergleich zeigt, dass unter den vorgestellten Technologien, Fog Computing die vielversprechendeste Form für den Einsatz in zukünftigen VANETs ist.
Der Nachteil der geringeren Bandbreite kann durch die Kombination des WAVE-Standards mit Mobilfunktechnologien ausgeglichen werden.
Daher soll der Einsatz von Fog Computing und die Kombination mit \ac{SDN} im Folgenden genauer beleuchtet werden.

\subsection{Fog Computing und \acf{SDN} in VANETs}
Aufgrund des geringen Abstands zwischen den Fahrzeugen, die als Datensammler dienen, und den Edge Server, welche die Datenverarbeitung und Entscheidungsfindung ausführen, ermöglicht Fog Computing die Auswertung von Daten in Echtzeit.
Zudem können ausgewählte, aggregierte Daten zur zentralen Cloud, welche komplexere Analysen ausführen kann, durchgestellt werden.\\
Falls die Verbindungsqualität zwischen einem Fog Server und einem Fahrzeug, aufgrund dessen Bewegung, abnimmt, kann die Ausführung der Anwendung auf die nächstgelegene Fog Server-Instanz übergeben werden.
Zusätzlich können die Fog Server auch autonom arbeiten, wenn die Verbindung zur zentralen Cloud nicht vorhanden ist, indem sie Daten untereinander austauschen.\\
Außerdem gibt es vielfältige Möglichkeiten Datenschutz umzusetzen, da die Fahrzeuginhaber selbst entscheiden können, an welche Ziele die Daten ihrer Fahrzeuge gesendet werden sollen.\\
Weiterhin gibt es die Herausforderung, dass auch Fog Computing eine heterogene Systemlandschaft umfasst. 
Um dieser Herausforderung gerecht zu werden, kann \ac{SDN} eingesetzt werden.

\ac{SDN} dient zur zentralen Kontrolle und Programmierung des Netzwerkes.
Diese Technologie basiert auf dem OpenFlow-Protokoll, welches zwischen Daten- und Kontrollebene unterscheidet.
Die Kontrollebene auf der einen Seite kontrolliert und steuert den Datenfluss im Netzwerk.
Die Datenebene auf der anderen Seite enthält Informationen über den Netzwerkstatus und führt die Weiterleitung der Daten aus.
In Abbildung~\ref{fig:sdn} ist der Aufbau einer Infrastruktur mit \ac{SDN} schematisch dargestellt.\\
\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.44\linewidth]{./img/sdn.png}
    \caption{Schematische Darstellung von \acf{SDN} \cite{gazi_challenges_2018}}
    \label{fig:sdn}
\end{figure}

Der zentrale Bestandteil ist der \ac{SDN}-Controller.
Dieser kommuniziert über die \textit{northbound}-Schnittstelle mit den Anwendungen, die dem Benutzer erlauben das Netzwerk zu verwalten.\\
Über die sogenannte \textit{southbound}-Schnittstelle tauscht der \ac{SDN}-Controller Daten mit den Netzwerkgeräten aus.
Letztere müssen das OpenFlow-Protokoll unterstützen und Vorgaben des \ac{SDN}-Controllers zur Einrichtung von Routen akzeptieren.
Durch die Einrichtung von vordefinierten Routen können der Aufwand bei der Paketverarbeitung reduziert, die Netzwerkeffizienz gesteigert und damit die Betriebskosten gesenkt werden.
Netzwerkgeräte, die das OpenFlow-Protokoll unterstützten, verwenden Flusstabellen, die festhalten, ob Pakete verarbeitet oder weitergeleitet werden sollen.
Ein Switch prüft beispielsweise anhand des Headers der eintreffenden Nachricht in der Flusstabelle nach und leitet das Paket, falls bekannt, an die angegebene Adresse weiter.
Anderenfalls wird es an den \ac{SDN}-Controller zur weiteren Verarbeitung gesendet.\\
Der Mobilitätsmanager als Teil des \ac{SDN}-Controllers, stellt sicher, dass immer die aktuellen Status der Switches, \ac{RSU}s und Fahrzeuge vorliegen.
Dabei kümmert er sich auch um die Positionsvorhersage für Fahrzeuge, falls die Verbindung für kurze Zeit unterbrochen wird.
Die Netzwerktopologie wird durch den Routingmanager verwaltet.
Seine Arbeitslast ist somit abhängig von der Anzahl und Mobilität der Fahrzeuge. \\
Somit eignet sich \ac{SDN} für den Einsatz in VANETs um das Netzwerk zu flexibilisieren und damit geringe Latenz sicherzustellen.
Dies zeigt sich auch daran, dass es bereits mehrere Arbeiten gibt, die den Einsatz von \ac{SDN} in VANETs untersuchen.
Ein Beispiel dafür ist \textit{\ac{SVAO}}, wobei die Kontrollebene und Datenebene getrennt werden, um die Datenübetragungseffizienz zu erhöhen.
Während eine globale, verteilte Ebene Fahrzeugdaten sammelt, kümmert sich eine zentralisierte, lokale Ebene um die Auswahl der Geräte zur Weiterleitung von Nachrichten. \cite{dong_svao_2016}

\subsection{Beispielszenario zu Fog Computing in VANETs}
Im Folgenden soll beispielhaft der Einsatz von Fog Computing und \ac{SDN} zur Verteilung einer \textit{Safety}-Nachricht in VANETs betrachtet werden.
Die dazu notwendige Infrastruktur ist in Abbildung~\ref{fig:scenario} dargestellt.
\begin{figure}[h]
	\centering
	\includegraphics[width=0.99\linewidth]{./img/scenario.png}
    \caption{Verteilung verschiedener Nachrichtentypen in VANETs mit Fog Computing und \ac{SDN} \cite{gazi_challenges_2018}}
    \label{fig:scenario}
\end{figure}
In der Mitte dieser befinden sich die Fog Server, welche geografisch verteilt sind und andere Fog-Geräte über Kabel anbinden.
Da diese sowohl Rechenleistung, als auch Speicherplatz bereitstellen, muss nicht der gesamte Datenfluss zur zentralen Cloud führen, wodurch Betriebskosten gespart werden können.
Der \ac{SDN}-Controller befindet sich in dieser Architektur zwischen den Fog Servern und der zentralen Cloud.\\
Nimmt man nun, wie im Beispiel dargestellt, an, dass ein Unfall auftritt, so registriert ein Fahrzeug dies.
Folglich sendet es eine \textit{Safety}-Nachricht, die den Ereignistyp, z. B. Auffahrunfall, enthält, an die nahegelegenen RSUs und benachbarten Fahrzeuge. \\
Aufgrund der Broadcast-IP-Adresse wird die Nachricht von der RSU über den Switch und Router zum Fog Server weitergeleitet.
Letzterer berechnet anhand des empfangenen Ereignistyps und anderer Umgebungsbedingungen, wie z. B. Wetter, die Broadcastreichweite oder leitet die Nachricht an die zentrale Cloud für komplexere Auswertungen weiter.\\
Im ersten Fall versendet der Fog Server die berechnete Antwort gegebenfalls auch an benachbarte Fog Server.
Hierbei kann \ac{SDN} eingesetzt werden, um Routen zwischen den Fog Servern schon im Voraus aufzubauen.
Dazu wird in die Routing-Tabelle des Routers, der durch den \ac{SDN}-Controller verwaltet wird, eine Spalte mit dem Ereignistyp eingefügt.
Dies ermöglicht die Weiterleitung der Nachricht abhängig vom Ereignistyp an benachbarte Fog Server, wobei die zuvor eingerichteten Routen eine geringe Latenz bei der Übertragung garantieren.\\
Falls eine Route nicht eingerichtet sein sollte, so wird die Nachricht an den Fog Server gesendet.
Dieser ermittelt die entsprechende Zieladresse und teilt sie dem \ac{SDN}-Controller mit.
Zudem kann der \ac{SDN}-Controller den Router so einstellen, dass das zuvor fehlgeleitete Paket priorisiert wird.
Ohne den Einsatz von \ac{SDN} könnten keine im Voraus eingerichteten Routen genutzt werden, womit die Latenz und der Durchsatz geringer wären. 
